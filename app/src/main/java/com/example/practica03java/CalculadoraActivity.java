package com.example.practica03java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private Button btnSumar;
    private Button btnRestar;
    private Button btnMulti;
    private Button btnDiv;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtUno;
    private EditText txtDos;

    // Declarar el objeto calculadora
    private Calculadora calculadora = new Calculadora(0, 0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();
        // Obtener datos del MainActivity
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);

        // Evento click para cada botón
        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSumar();
            }
        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRestar();
            }
        });
        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMultiplicar();
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDividir();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLimpiar();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRegresar();
            }
        });
    }

    // Función para iniciar los componentes
    private void iniciarComponentes() {
        //Botones
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnMulti = findViewById(R.id.btnMultiplicar);
        btnDiv = findViewById(R.id.btnDividir);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        //Etiquetas
        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);

        //Cajas de texto
        txtUno = findViewById(R.id.txtNum1);
        txtDos = findViewById(R.id.txtNum2);
    }

    private void btnSumar() {
        if(txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()){
            Toast.makeText(this.getApplicationContext(), "Ingrese un número por favor",
                    Toast.LENGTH_SHORT).show();
        } else{
            calculadora.num1 = Float.parseFloat(txtUno.getText().toString());
            calculadora.num2 = Float.parseFloat(txtDos.getText().toString());
            float total = calculadora.suma();
            lblResultado.setText(String.valueOf(total));
        }
    }

    private void btnRestar() {
        if(txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()){
            Toast.makeText(this.getApplicationContext(), "Ingrese un número por favor",
                    Toast.LENGTH_SHORT).show();
        } else{
            calculadora.num1 = Float.parseFloat(txtUno.getText().toString());
            calculadora.num2 = Float.parseFloat(txtDos.getText().toString());
            float total = calculadora.resta();
            lblResultado.setText(String.valueOf(total));
        }
    }

    private void btnMultiplicar() {
        if(txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()){
            Toast.makeText(this.getApplicationContext(), "Ingrese un número por favor",
                    Toast.LENGTH_SHORT).show();
        } else{
            calculadora.num1 = Float.parseFloat(txtUno.getText().toString());
            calculadora.num2 = Float.parseFloat(txtDos.getText().toString());
            float total = calculadora.multiplicacion();
            lblResultado.setText(String.valueOf(total));
        }
    }

    private void btnDividir() {
        if(txtUno.getText().toString().isEmpty() || txtDos.getText().toString().isEmpty()){
            Toast.makeText(this.getApplicationContext(), "Ingrese un número por favor",
                    Toast.LENGTH_SHORT).show();
        } else{
            calculadora.num1 = Float.parseFloat(txtUno.getText().toString());
            calculadora.num2 = Float.parseFloat(txtDos.getText().toString());
            float total = calculadora.division();
            lblResultado.setText(String.valueOf(total));
        }
    }

    private void btnLimpiar() {
        if(txtUno.getText().toString().isEmpty() && txtDos.getText().toString().isEmpty()
            && lblResultado.getText().toString().isEmpty()){
            Toast.makeText(this.getApplicationContext(), "No hay información para limpiar",
                    Toast.LENGTH_SHORT).show();
        } else{
            lblResultado.setText("");
            txtUno.setText("");
            txtDos.setText("");
        }
    }

    private void btnRegresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //No hace nada
            }
        });
        confirmar.show();
    }

}