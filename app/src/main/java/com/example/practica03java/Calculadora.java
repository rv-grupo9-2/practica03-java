package com.example.practica03java;

public class Calculadora {
    float num1 = 0;
    float num2 = 0;

    // Constructor de parámetros
    public Calculadora(float numero1, float numero2) {
        this.num1 = numero1;
        this.num2 = numero2;
    }

    // Función sumar
    public float suma() {
        return num1+num2;
    }

    // Función restar
    public float resta() {
        return num1-num2;
    }

    // Función multiplicar
    public float multiplicacion() {
        return num1*num2;
    }

    // Función dividir
    public float division() {
        float total = 0;
        if(num2!=0) {
            total = num1/num2;
        }
        return total;
    }
}
